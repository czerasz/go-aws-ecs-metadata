package metadata

import (
	"testing"
)

var s = V2Metadata{
	Containers: []Container{
		Container{
			Name:        "pipeline",
			KnownStatus: StatusStopped,
			ExitCode:    1,
		},
		Container{
			Name:        "metrics",
			KnownStatus: StatusRunning,
		},
	},
}

func TestContainerByName(t *testing.T) {
	name := "pipeline"
	c := s.containerByName(name)
	if c.Name != name {
		t.Errorf("wrong container returned, got: %s, want: %s", c.Name, name)
	}

	name = "unknown"
	c = s.containerByName(name)
	if c != nil {
		t.Errorf("expected nil for \"unknown\" container name")
	}
}
