# Golang AWS ECS metadata SDK

Get information about containers running as an AWS ECS task.

## Usage

```golang
defaultTimeout := 5
client := &http.Client{
  Timeout: time.Duration(defaultTimeout) * time.Second,
}

meta, err := metadata.FetchV2Metadata(client, c.MetadataEndpoint)

if err != nil {
  log.Fatalf("could not fetch metadata: %s", err)
}
```