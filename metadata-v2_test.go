package metadata_test

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"

	metadata "gitlab.com/czerasz/go-aws-ecs-metadata"
)

var s = metadata.V2Metadata{
	Containers: []metadata.Container{
		metadata.Container{
			Name:        "pipeline",
			KnownStatus: metadata.StatusStopped,
			ExitCode:    1,
		},
		metadata.Container{
			Name:        "metrics",
			KnownStatus: metadata.StatusRunning,
		},
	},
}

func TestFetchV2Metadata(t *testing.T) {
	path := fmt.Sprintf("/%s/metadata", metadata.AwsECSTaskMetadataEndpointVersion)

	// Start a local HTTP server
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		// Test request parameters
		equals(t, req.URL.String(), path)

		data, err := ioutil.ReadFile("metadata-response.mock.json")
		ok(t, err)

		w.Header().Add("Content-Type", "application/json")
		w.Write(data)
	}))
	// Close the server when test finishes
	defer server.Close()

	meta, err := metadata.FetchV2Metadata(server.Client(), server.URL)
	ok(t, err)
	equals(t, meta.Containers[0].Name, "monitoring")
}

func TestFetchV2MetadataWithWrongContentType(t *testing.T) {
	path := fmt.Sprintf("/%s/metadata", metadata.AwsECSTaskMetadataEndpointVersion)

	// Start a local HTTP server
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		// Test request parameters
		equals(t, req.URL.String(), path)

		data, err := ioutil.ReadFile("metadata-response.mock.json")
		ok(t, err)

		w.Header().Add("Content-Type", "text/plain")
		w.Write(data)
	}))
	// Close the server when test finishes
	defer server.Close()

	_, err := metadata.FetchV2Metadata(server.Client(), server.URL)
	if err == nil || err != metadata.ErrWrongContentType {
		t.Errorf("no error about invalid content type returned")
	}
}

func TestIsContainerStopped(t *testing.T) {
	stopped, exitCode, err := s.IsContainerStopped("unknown")
	if err == nil {
		t.Errorf("unknown container should NOT exist")
	}

	stopped, exitCode, err = s.IsContainerStopped("pipeline")
	if err != nil {
		t.Errorf("pipeline container should exist")
	}
	if !stopped {
		t.Errorf("pipeline container should be stopped")
	}
	if exitCode != 1 {
		t.Errorf("pipeline container should have the correct exit code")
	}

	stopped, exitCode, err = s.IsContainerStopped("metrics")
	if err != nil {
		t.Errorf("metrics container should exist")
	}
	if stopped {
		t.Errorf("metrics container should NOT be stopped")
	}
	if exitCode != 0 {
		t.Errorf("metrics container should have the correct exit code")
	}

	_, _, err = s.IsContainerStopped("unknown")
	if !errors.Is(err, metadata.ErrContainerNotExist) {
		t.Errorf("unexpected error: %s", err)
	}

}

// ok fails the test if an err is not nil.
// Resource: https://github.com/benbjohnson/testing
func ok(tb testing.TB, err error) {
	if err != nil {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("%s:%d: unexpected error: %s\n", filepath.Base(file), line, err.Error())
		tb.FailNow()
	}
}

// equals fails the test if exp is not equal to act.
// Resource: https://github.com/benbjohnson/testing
func equals(tb testing.TB, exp, act interface{}) {
	if !reflect.DeepEqual(exp, act) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\texp: %#v\n\n\tgot: %#v\033[39m\n\n", filepath.Base(file), line, exp, act)
		tb.FailNow()
	}
}
