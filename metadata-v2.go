package metadata

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
)

const StatusStopped = "STOPPED"
const StatusRunning = "RUNNING"

const AwsECSTaskV2MetadataEndpointHost = "169.254.170.2"
const AwsECSTaskMetadataEndpointVersion = "v2"

type Container struct {
	Name        string
	DockerName  string
	KnownStatus string
	ExitCode    int
}

type V2Metadata struct {
	Containers         []Container
	Cluster            string
	TaskARN            string
	Family             string
	Revision           string
	DesiredStatus      string
	KnownStatus        string
	PullStartedAt      string
	PullStoppedAt      string
	ExecutionStoppedAt string
	AvailabilityZone   string
}

var ErrWrongContentType = errors.New("response not returned as JSON, wrong Content-Type header")
var ErrContainerNotExist = errors.New("container does not exist")

func (s *V2Metadata) containerByName(name string) *Container {
	for _, c := range s.Containers {
		if c.Name == name {
			return &c
		}
	}

	return nil
}

// IsContainerStopped returns true if container is in STOPPED state
func (s *V2Metadata) IsContainerStopped(name string) (bool, int, error) {
	c := s.containerByName(name)

	if c == nil {
		return false, 0, fmt.Errorf("container \"%s\": %w", name, ErrContainerNotExist)
	}

	return c.KnownStatus == StatusStopped, c.ExitCode, nil
}

// FetchV2Metadata returns metadata about containers
func FetchV2Metadata(c *http.Client, baseURL string) (*V2Metadata, error) {
	if baseURL == "" {
		baseURL = fmt.Sprintf("http://%s", AwsECSTaskV2MetadataEndpointHost)
	}

	path := fmt.Sprintf("/%s/metadata", AwsECSTaskMetadataEndpointVersion)

	req, err := http.NewRequest(http.MethodGet, baseURL+path, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Accept", "application/json")

	res, err := c.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.Header.Get("Content-Type") != "application/json" {
		return nil, ErrWrongContentType
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	var s V2Metadata
	err = json.Unmarshal(body, &s)

	if err != nil {
		return nil, err
	}

	return &s, nil
}
