# [0.2.0](https://gitlab.com/czerasz/go-aws-ecs-metadata/compare/v0.1.0...v0.2.0) (2020-07-30)


### Continuous Integrations

* configure CI ([f1316b0](https://gitlab.com/czerasz/go-aws-ecs-metadata/commit/f1316b050af6ccc5610510b6dc22cf8188bd6615))


### Documentation

* add documentation ([ba0cfae](https://gitlab.com/czerasz/go-aws-ecs-metadata/commit/ba0cfae39e2238760edad35e1fc9d335d1989b0d))


### Features

* add logic ([47c1ebf](https://gitlab.com/czerasz/go-aws-ecs-metadata/commit/47c1ebfc43f0b3bb1454ec4226bac1fdc700b019))
